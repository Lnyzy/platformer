﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    // Play the game
    public void PlayGameAgain ()
    {
        SceneManager.LoadScene("Game");
    }
}
