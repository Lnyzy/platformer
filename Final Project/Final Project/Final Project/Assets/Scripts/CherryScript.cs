﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherryScript : MonoBehaviour
{
    public AudioClip CherryCrunch;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Add 1 to Lives
            other.GetComponent<LifeScript>().lives++;
            AudioSource.PlayClipAtPoint(CherryCrunch, transform.position);
            //Destroy Cherry after pickup
            Destroy(this.gameObject);
        }
    }
}
