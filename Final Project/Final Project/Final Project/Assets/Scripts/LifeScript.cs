﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeScript : MonoBehaviour
{
    public int lives = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnGUI()
    {
        // My Label for lives
        GUI.color = Color.black;
        GUI.Label(new Rect(10, 30, 100, 20), "Lives : " + lives);
    }
}
