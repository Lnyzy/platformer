﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemScript : MonoBehaviour
{
    public AudioClip GemSound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Add 1 to Points
            other.GetComponent<PointScript>().points++;
            AudioSource.PlayClipAtPoint(GemSound, transform.position);
            //Destroy Gem after pick up
            Destroy(this.gameObject);
        }
    }
}
